# Présentation

Site web pour présenter le projet « Carnet De Jeu ».

# Licence

Cette page est délivrée sous les termes de la licence publique WTF. Pour plus d'informations, veuillez lire la [licence WTFP](./LICENSE).
