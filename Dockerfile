FROM caddy:2-alpine

COPY Caddyfile /etc/caddy/Caddyfile
COPY index.html /usr/share/caddy/
COPY bulma.min.css /usr/share/caddy/
COPY fa-5.1.0.js /usr/share/caddy/
